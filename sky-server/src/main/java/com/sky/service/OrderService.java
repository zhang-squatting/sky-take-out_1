package com.sky.service;

import com.sky.dto.*;
import com.sky.result.PageResult;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import org.springframework.stereotype.Service;

@Service
public interface OrderService {
     PageResult conditionSearch(OrdersPageQueryDTO ordersPageQueryDTO) ;


    OrderSubmitVO ordersSubmit(OrdersSubmitDTO ordersSubmitDTO);

   OrderPaymentVO payment(OrdersPaymentDTO ordersPaymentDTO) throws Exception;

    void paySuccess(String outTradeNo);

    PageResult pageQuery4User(int page, int pageSize, Integer status);

    OrderVO details(Long id);

    void getByCancelId(Long id) throws Exception;

    void repetition(Long id);

    void orderconfirm(OrdersConfirmDTO ordersConfirmDTO);

    void orderJection(OrdersRejectionDTO ordersRejectionDTO) throws Exception;

    OrderStatisticsVO statistics();

    void cancel(OrdersCancelDTO ordersCancelDTO) throws Exception;

    void delivery(Long id);

    void complete(Long id);

}
