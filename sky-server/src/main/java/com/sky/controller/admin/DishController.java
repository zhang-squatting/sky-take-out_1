package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.service.impl.DishServiceImpl;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/dish")
@Slf4j
@Api(tags = "菜品相关接口")
public class DishController {

    @Autowired
    private DishService dishService;

    @PostMapping
    @ApiOperation("新增菜品")
    public Result saveWithDishFlovors(@RequestBody DishDTO dishDTO){
        //1.接收参数
        log.info("新增菜品参数：{}",dishDTO);
        //2.调用service层方法，处理业务逻辑
        dishService.saveWithDishFlovors(dishDTO);
        //3.响应数据
        return Result.success();
    }

    @GetMapping("/page")
    @ApiOperation("菜品分页查询")
    public Result<PageResult> page(DishPageQueryDTO dishPageQueryDTO){
        //1.接收参数
        log.info("分页查询参数:{}",dishPageQueryDTO);
        //2.调用service层方法，处理业务逻辑
        PageResult pageResult = dishService.page(dishPageQueryDTO);
        //3.响应数据
        return Result.success(pageResult);
    }

    @DeleteMapping
    @ApiOperation("菜品的批量删除")
    // public Result delete(String ids){
    public Result delete(@RequestParam List<Long> ids){
        //1.接收参数
        log.info("批量删除参数：{}",ids);
        //2.调用service层方法，处理业务逻辑
        dishService.deleteBatch(ids);
        //3.响应数据
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询菜品")
    public Result<DishVO> getById(@PathVariable Long id){
        //1.接收参数
        log.info("获取id参数：{}",id);
        //2.调用service层方法，处理业务逻辑
        DishVO dishVO = dishService.getById(id);
        //3.响应数据
        return Result.success(dishVO);
    }

    @PutMapping
    public Result update(@RequestBody DishDTO dishDTO){
        //1.接收参数
        log.info("获取修改参数：{}",dishDTO);
        //2.调用service层方法，处理业务逻辑
        dishService.updateById(dishDTO);
        //3.响应数据
        return Result.success();
    }

    @PostMapping("/status/{status}")
    @ApiOperation("菜品起售停售")
    public Result startOrStop(@PathVariable Integer status,Long id){
        log.info("接收参数：{}，{}",status,id);
        dishService.startOrStop(status,id);

        return Result.success();
    }

    /**
     * 根据分类id查询菜品
     * @param categoryId
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("根据分类id查询菜品")
    public Result<List<Dish>> list(Long categoryId){
        List<Dish> list = dishService.list(categoryId);
        return Result.success(list);
    }
}
